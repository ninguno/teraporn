'use strict'

// Teraporn server.js
const fs = require('fs'),
    http = require('http'),
    zlib = require('zlib'),
    ffmpeg = require('fluent-ffmpeg'),
    config = require('./config.json'),
    random = array => {
        if (Array.isArray(array))
            return Math.floor(Math.random() * array.length)
        else return 0
    },
    kill = (ffm, ffmgif) => {
        if (ffm)
            ffm.kill()
        if (ffmgif)
            ffmgif.kill()
    },
    videos = fs.readdirSync(config.dir).filter(video => {
        if (video.match(/\.mp4$/) || video.match(/\.avi$/))
            return video
    }),
    fonts = [],
    loadGif = (req, res, ffm, ffmgif) => {
        if (req.url.match(/^\/teraporn.gif(\?t=[0-9]+)?$/)) {
            let file = videos[random(videos)]
            res.writeHead(200, { 'Content-Type': 'image/gif', 'Cache-Control': 'no-cache', 'Connection': 'close', 'content-encoding': 'gzip' })
            ffm = ffmpeg(config.dir + '/' + file)
            ffm.on('error', (err, stdout, stderr) => {
                console.log('Cannot process gif: ' + err.message + '\n' + stdout + '\n' + stderr)
                kill(ffm, ffmgif)
                res.end('Error loading video')
            })
                .ffprobe((err, metadata) => {
                    if (err || !metadata) {
                        console.log('Cannot process gif: ' + err.message)
                        kill(ffm, ffmgif)
                        res.end('Error loading video')
                    }
                    else {
                        let chunk = config.secondsGif,
                            duration = Math.floor(metadata.streams[0].duration),
                            start = 1,
                            end = duration + 1
                        while (end > duration) {
                            start = Math.floor(Math.random() * duration) + 1
                            end = start + chunk
                        }
                        let colors = config.colors,
                            randomcolor = random(colors),
                            randomfont = random(fonts),
                            bgcolor = colors[randomcolor]
                        while (bgcolor === colors[randomcolor])
                            bgcolor = colors[random(colors)]
                        ffm
                            .duration(chunk)
                            .seekInput(start)
                            .fps(15)
                            .size('320x?')
                            .aspect('4:3')
                            .autopad()
                            .outputFormat('avi')
                            .noAudio()
                        ffmgif = ffmpeg(ffm.pipe())
                        ffmgif.on('error', (err, stdout, stderr) => {
                            console.log('Cannot process gif: ' + err.message + '\n' + stdout + '\n' + stderr)
                            kill(ffm, ffmgif)
                            res.end('Error loading video')
                        })
                            .videoFilters('drawtext=fontfile=' + fonts[randomfont] + ':text=TeraPorn:fontcolor=' + colors[randomcolor] + '@0.8:fontsize=24:x=5:y=5:box=1:boxcolor=' + bgcolor + '@0.5')
                            .outputOptions(['-gifflags +transdiff'])
                            .outputFormat('gif')
                            .pipe(zlib.createGzip()).pipe(res, { end: true })
                    }
                })
            setTimeout(() => {
                kill(ffm, ffmgif)
                res.end('Error loading image')
            }, config.secondsTimeout * 1000)
        }
        else {
            res.writeHead(301, { Location: '/teraporn.gif' })
            res.end()
        }
    }
http.createServer((req, res) => {
    let ffm, ffmgif
    req.on('error', err => {
        console.log(err)
        kill(ffm, ffmgif)
    })
    res.on('error', err => {
        console.log(err)
        kill(ffm, ffmgif)
    })
    loadGif(req, res, ffm, ffmgif)
}).listen(config.port)

fs.readdir(config.fontsdir, (err, fontdir) => {
    if (err) console.log(err)
    else {
        fontdir.map(font => {
            if (fs.lstatSync(config.fontsdir + '/' + font).isDirectory()) {
                fs.readdir(config.fontsdir + '/' + font, (err, files) => {
                    if (err) console.log(err)
                    else {
                        files.map(file => {
                            if (file.match(/\.ttf$/))
                                fonts.push(config.fontsdir + '/' + font + '/' + file)
                        })
                    }
                })
            }
        })
    }
})